reniced
=======

Daemon to automatically scan and renice processes on a linux box.

# Installation
Copy 'reniced.php' to a directory you like. For example /usr/local/sbin.

Copy reniced.conf to /etc.

Start like ```nohup /usr/local/sbin/php /path/to/reniced.php > /var/log/reniced.log &``` as root (or possibly via sudo).
I recommend putting this in your rc.local.
