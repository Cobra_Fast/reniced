#!/usr/bin/php
<?php

// define default config
$dbg = false;
$conf = array();
$ioconf = array();
$cycleBreak = 1000000;
$procBreak = 125000;
$pcache_lifetime = 5;

// internal variables
$pcache = array();

// helper functions
function sign($i){return ($i >= 0 ? '+' . $i : (string)$i);}

function setNice($pid, $n)
{
    global $pcache, $pcache_lifetime, $dbg;
    if (isset($pcache[$pid]) && ($pcache[$pid] + $pcache_lifetime) > time())
        return false;
    
if ($dbg) echo 'Renicing ' , $pid , ' to ' , sign($n) , "\n";
    shell_exec('renice ' . sign($n) . ' -p ' . escapeshellarg($pid));
    $pcache[$pid] = time();
    return true;
}

function setIONice($pid, $a)
{
    global $pcache, $pcache_lifetime, $dbg;
    if (isset($pcache[$pid]) && ($pcache[$pid] + $pcache_lifetime) > time())
        return false;

if ($dbg) echo 'IORenicing ' , $pid , ' to ' , $inice , "\n";
    shell_exec('ionice -p ' . escapeshellarg($pid) . ' ' . $a);
    $pcache[$pid] = time();
    return true;
}

// program starts here
while (true)
{
    $time = time();
    
    // reload config if valid
    if ($time % $pcache_lifetime == 0 && strpos(shell_exec('/usr/bin/php -l /etc/reniced.conf 2>&1'), 'No syntax errors') !== false)
    {
if ($dbg) echo 'Reloading configuration.' , "\n";
        @include('/etc/reniced.conf');
    }

    // clean pid cache
    if ($time % $pcache_lifetime == 0)
    {
        foreach ($pcache as $pid => $ctime)
            if (($ctime + $pcache_lifetime) > time())
                unset($pcache[$pid]);
    }

    // compare processes and renice them if applicable
    $procs = glob('/proc/*', GLOB_ONLYDIR);
    foreach ($procs as $proc)
    {
        $pid = basename($proc);
        if (!is_numeric($pid) || !is_file($proc . '/cmdline')) continue;
        
        $cmdline = str_replace("\0", ' ', file_get_contents($proc . '/cmdline'));
        $cpid = array();
        foreach ($conf as $cproc => $cnice)
        {
            if (strpos($cmdline, $cproc) !== false && is_dir($proc))
            {
                if (setNice($pid, $cnice) && is_dir($proc . '/task'))
                {
                    $cpid = array_map('basename', glob($proc . '/task/*', GLOB_ONLYDIR));
                    foreach ($cpid as $cp)
                    {
                        if (in_array($cp, $procs)) continue;
                        setNice($cp, $cnice);
                    }
                }
            }
        }

        foreach ($ioconf as $iproc => $inice)
        {
            if (strpos($cmdline, $iproc) !== false && is_dir($proc))
            {
                if (setIONice($pid, $inice) && is_dir($proc . '/task'))
                {
                    $cpid = array_map('basename', glob($proc . '/task/*', GLOB_ONLYDIR));
                    foreach ($cpid as $cp)
                    {
                        if (in_array($cp, $procs)) continue;
                        setIONice($cp, $inice);
                    }
                }
            }
        }
        
        if ($procBreak > 0)
            usleep($procBreak);
    }

if ($dbg) echo 'Cycle elapsed.' , "\n";
    if ($cycleBreak > 0)
        usleep($cycleBreak);
}
exit(0);